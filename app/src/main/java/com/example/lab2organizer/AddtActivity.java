package com.example.lab2organizer;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.app.AlarmManager;
import android.widget.Toast;

public class AddtActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addt);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setTask(View view) throws Exception {
        EditText taskTitleView = findViewById(R.id.task_title);
        EditText taskTextView = findViewById(R.id.task_text);

        DatePicker datePicker = findViewById(R.id.date_picker);
        TimePicker timePicker = findViewById(R.id.time_picker);

        Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                datePicker.getMonth(),
                datePicker.getDayOfMonth(),
                timePicker.getHour(),
                timePicker.getMinute());

        long taskTime = calendar.getTimeInMillis();
        String taskTitle = taskTitleView.getText().toString();
        String taskText = taskTextView.getText().toString();
        Date datetime = calendar.getTime();

        try {
            String resInsert = this.insertTask(taskTime, taskTitle, taskText);
            if (resInsert != null) {
                int int_random = ThreadLocalRandom.current().nextInt();

                Intent notifyIntent = new Intent(this,TaskReceiver.class);
                notifyIntent.putExtra("taskTitle", taskTitle);
                notifyIntent.putExtra("taskText", taskText);
                notifyIntent.putExtra("taskFile", resInsert);
                notifyIntent.putExtra("taskTime", int_random);

                PendingIntent pendingIntent = PendingIntent.getBroadcast
                        (this, int_random, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                alarmManager.setExact(AlarmManager.RTC_WAKEUP,taskTime,pendingIntent);
//                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,  System.currentTimeMillis(),
//                        1000 * 60 * 60 * 24, pendingIntent);

                Toast.makeText(getApplicationContext(),
                        "Evenimentul a fost setat cu succes",
                        Toast.LENGTH_LONG).show();

                Intent myIntent = new Intent(this, MainActivity.class);
                startActivity(myIntent);
                finish();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public String insertTask(long taskTime, String taskTitle, String taskText) throws Exception {
//        InputStream fXmlFile = getAssets().open("tasks.xml");
////        InputStream fXmlFile = openFileInput("tasks.xml");
//        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//        Document doc = dBuilder.parse(fXmlFile);
//        doc.getDocumentElement().normalize();
////        NodeList tasks = doc.getElementsByTagName("task");
//
//        // find root
//        NodeList rootList = doc.getElementsByTagName("task");
//        Node root = rootList.item(0);
//
//        // append using a helper method
//        root.appendChild(createEmployee(doc, Long.toString(taskTime), taskTitle, taskText));

        Serializer serializer = new Persister();
        Task example = new Task(taskTitle, taskText, taskTime);
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        long tsLong = System.currentTimeMillis()/1000;
        String ts = Long.toString(tsLong);
        File result = new File(path,"/tasks");
        if (!result.exists()) {
            if(!result.mkdirs()) {
                return null;
            }
        }

        String fileName = "task_" + ts + ".xml";
        File xmlFile = new File(result, fileName);
        serializer.write(example, xmlFile);

////        File myDir = getExternalStorageDirectory();
//        String filename = "tasks.xml";
//        FileOutputStream fos;
////        FileOutputStream fos = new FileOutputStream(file);
//        fos = openFileOutput(filename, Context.MODE_APPEND);
//
//        XmlSerializer serializer = Xml.newSerializer();
//        serializer.setOutput(fos, "UTF-8");
//        serializer.startDocument(null, Boolean.valueOf(true));
//        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
//        serializer.startTag(null, "tasks");
//
//        serializer.startTag(null, "task");
//        serializer.startTag(null, "name");
//        serializer.text("tralala");
//        serializer.endTag(null, "name");
//        serializer.endTag(null, "task");
//
//        serializer.endTag(null, "tasks");
//
//        serializer.endDocument();
//        serializer.flush();
//        fos.close();
        return fileName;
    }

    public Element createEmployee(Document doc, String taskTime, String taskTitle, String taskText) {
        // create new Employee
        Element task = doc.createElement("task");
//        task.setAttribute("gender", gender);

        // create child nodes
        Element time = doc.createElement("taskTime");
        time.appendChild(doc.createTextNode(taskTime));

        Element title = doc.createElement("title");
        title.appendChild(doc.createTextNode(taskTitle));

        Element text = doc.createElement("text");
        text.appendChild(doc.createTextNode(taskText));

        // append and return
        task.appendChild(time);
        task.appendChild(title);
        task.appendChild(text);

        return task;
    }
}