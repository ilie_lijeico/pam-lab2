package com.example.lab2organizer;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.view.View.*;

public class MainActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handleIntent(getIntent());

        // tasks layout
        final LinearLayout lm = findViewById(R.id.organizer_listing);
        // create the layout params that will be used to define how your
        // button will be displayed
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(36, 2, 2, 60);

        // get tasks
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        final File folder = new File(path,"/tasks");
        File[] files = folder.listFiles();
        if (files != null) {
            for (final File file : files) {
                Serializer serializer = new Persister();
                File source = new File(folder, file.getName());
                try {
                    final Task task = serializer.read(Task.class, source);

                    Date date = new Date(task.datetime);
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    final String dateFormatted = formatter.format(date);

                    // Create LinearLayout
                    LinearLayout ll = new LinearLayout(this);
                    ll.setOrientation(LinearLayout.VERTICAL);
                    ll.setPadding(50, 0, 50, 0);

                    // Create TextView
                    TextView product = new TextView(this);
                    product.setText(task.title);
                    product.setTypeface(null, Typeface.BOLD);
                    ll.addView(product);

                    // Create TextView
                    TextView price = new TextView(this);
                    price.setText(task.text);
                    ll.addView(price);

                    // Create TextView
                    TextView datetime = new TextView(this);
                    datetime.setText(dateFormatted);
                    ll.addView(datetime);

                    LinearLayout bb = new LinearLayout(this);
                    bb.setOrientation(LinearLayout.HORIZONTAL);
                    bb.setHorizontalGravity(TEXT_ALIGNMENT_CENTER);

                    // Create Button
                    final Button btn = new Button(this);
                    btn.setTransitionName(file.getName());
                    btn.setText("Editeaza");
                    // set the layoutParams on the button
                    btn.setLayoutParams(params);

                    // Set click listener for button
                    final MainActivity currActivity = this;
                    btn.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            Intent myIntent = new Intent(currActivity, UpdateActivity.class);
                            myIntent.putExtra("taskDate", task.datetime);
                            myIntent.putExtra("taskTitle", task.title);
                            myIntent.putExtra("taskText", task.text);
                            myIntent.putExtra("filename", btn.getTransitionName());
                            startActivity(myIntent);
                        }
                    });

                    //Add button to LinearLayout
                    bb.addView(btn);

                    // Create Button
                    final Button btn2 = new Button(this);
                    btn2.setTransitionName(file.getName());
                    btn2.setText("Sterge");
                    // set the layoutParams on the button
                    btn2.setLayoutParams(params);

                    // Set click listener for button
                    btn2.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            File fileDelete = new File(folder, file.getName());
                            if (fileDelete.exists() && fileDelete.delete()) {
                                finish();
                                startActivity(getIntent());
                            }
                        }
                    });

                    //Add button to LinearLayout
                    bb.addView(btn2);
                    ll.addView(bb);

                    //Add button to LinearLayout defined in XML
                    lm.addView(ll);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow
        }
    }

    public void addTask(MenuItem item) {
        Intent myIntent = new Intent(this, AddtActivity.class);
        startActivity(myIntent);
    }
}