package com.example.lab2organizer;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Task {

    @Element
    public String title;

    @Element
    public String text;

    @Element
    public long datetime;

//    @Attribute
//    private int index;

    public Task() {
        super();
    }

    public Task(String title, String text, long datetime) {
        this.title = title;
        this.text = text;
        this.datetime = datetime;
    }

//    public Task(String text, int index) {
//        this.text = text;
//        this.index = index;
//    }

//    public String getMessage() {
//        return text;
//    }
//
//    public int getId() {
//        return index;
//    }
}
