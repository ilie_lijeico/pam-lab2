package com.example.lab2organizer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.concurrent.ThreadLocalRandom;

public class TaskReceiver extends BroadcastReceiver {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onReceive(Context context, Intent intent) {
        int int_random = ThreadLocalRandom.current().nextInt();

        Intent intent1 = new Intent(context, TaskService.class);
        intent1.putExtra("taskTitle", intent.getStringExtra("taskTitle"));
        intent1.putExtra("taskText", intent.getStringExtra("taskText"));
        intent1.putExtra("taskFile", intent.getStringExtra("taskFile"));
        intent1.putExtra("taskTime", intent.getIntExtra("taskTime", int_random));

        context.startService(intent1);
    }
}
