package com.example.lab2organizer;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.os.Environment;

import androidx.core.app.NotificationManagerCompat;

import java.io.File;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class TaskService extends IntentService {
    private static final int NOTIFICATION_ID = 3;

    public TaskService() {
        super("TaskService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String taskTitle = intent.getStringExtra("taskTitle");
        String taskText = intent.getStringExtra("taskText");
        String taskFile = intent.getStringExtra("taskFile");
        int taskTime = intent.getIntExtra("taskTime", 0);

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File currentTask = new File(path,"/tasks/" + taskFile);

        if (currentTask.exists() && currentTask.delete()) {
            Notification.Builder builder = new Notification.Builder(this);
            builder.setContentTitle(taskTitle);
            builder.setContentText(taskText);
            builder.setSmallIcon(R.drawable.ic_add);
            Intent notifyIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, taskTime, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            //to be able to launch your activity from the notification
            builder.setContentIntent(pendingIntent);
            Notification notificationCompat = builder.build();
            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);
            managerCompat.notify(taskTime, notificationCompat);

//            Intent myIntent = new Intent(this, MainActivity.class);
//            myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(myIntent);
        }
    }
}
