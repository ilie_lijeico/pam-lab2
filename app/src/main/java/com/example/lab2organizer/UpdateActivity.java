package com.example.lab2organizer;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import static java.lang.Integer.parseInt;

public class UpdateActivity extends AppCompatActivity {
    String filename = null;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        Intent intent = getIntent();
        String taskTitle = intent.getStringExtra("taskTitle");
        String taskText = intent.getStringExtra("taskText");
        long datetime = intent.getLongExtra("taskDate", 0);
        this.filename = intent.getStringExtra("filename");

        Date taskDate = new Date(datetime);

        EditText taskTitleView = findViewById(R.id.task_title);
        taskTitleView.setText(taskTitle);

        EditText taskTextView = findViewById(R.id.task_text);
        taskTextView.setText(taskText);

        DatePicker datePicker = findViewById(R.id.date_picker);
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String dateFormatted = formatter.format(taskDate);
        String[] datetimeSplit = dateFormatted.split(" ");
        String[] dateSplit = datetimeSplit[0].split("-");
        if (dateSplit.length == 3) {
            int Year = parseInt(dateSplit[0]);
            int Month = parseInt(dateSplit[1]) - 1;
            int Day = parseInt(dateSplit[2]);

            datePicker.updateDate(Year, Month, Day);
        }

        TimePicker timePicker = findViewById(R.id.time_picker);
        timePicker.setHour(taskDate.getHours());
        timePicker.setMinute(taskDate.getMinutes());
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void updateTask(View view) throws Exception {
        if (this.filename != null) {
            EditText taskTitleView = findViewById(R.id.task_title);
            EditText taskTextView = findViewById(R.id.task_text);

            DatePicker datePicker = findViewById(R.id.date_picker);
            TimePicker timePicker = findViewById(R.id.time_picker);

            Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                    datePicker.getMonth(),
                    datePicker.getDayOfMonth(),
                    timePicker.getHour(),
                    timePicker.getMinute());

            long taskTime = calendar.getTimeInMillis();
            String taskTitle = taskTitleView.getText().toString();
            String taskText = taskTextView.getText().toString();

            try {
                String resInsert = this.insertTask(taskTime, taskTitle, taskText);
                if (resInsert != null) {
                    int int_random = ThreadLocalRandom.current().nextInt();

                    Intent notifyIntent = new Intent(this,TaskReceiver.class);
                    notifyIntent.putExtra("taskTitle", taskTitle);
                    notifyIntent.putExtra("taskText", taskText);
                    notifyIntent.putExtra("taskFile", resInsert);
                    notifyIntent.putExtra("taskTime", int_random);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast
                            (this, int_random, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP,taskTime,pendingIntent);

                    Toast.makeText(getApplicationContext(),
                            "Evenimentul a fost editat cu succes",
                            Toast.LENGTH_LONG).show();

                    Intent myIntent = new Intent(this, MainActivity.class);
                    startActivity(myIntent);
                    finish();
                }
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            }
        }
    }

    public String insertTask(long taskTime, String taskTitle, String taskText) throws Exception {
        Serializer serializer = new Persister();
        Task example = new Task(taskTitle, taskText, taskTime);
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File result = new File(path,"/tasks");
        if (!result.exists()) {
            if(!result.mkdirs()) {
                return null;
            }
        }

        File xmlFile = new File(result, this.filename);
        serializer.write(example, xmlFile);

        return this.filename;
    }
}